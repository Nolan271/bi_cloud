package com.bi.cloud.dao;

import com.bi.cloud.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    Users getUsers(String username);
}
